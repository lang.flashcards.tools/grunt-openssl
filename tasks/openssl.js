/*
 * grunt-openssl
 * https://github.com/jdewit/grunt-openssl
 *
 * Copyright (c) 2014 Joris de Wit
 * Licensed under the MIT license.
 */

'use strict';

var crypto = require('crypto'),
    fs = require('fs');

module.exports = function(grunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks

  grunt.registerMultiTask('openssl', 'Encrypt files with openssl', function() {
    // Merge task-specific and/or target-specific options with these defaults.
    var options = this.options({
      salt: '',
      cipher: 'cast5-cbc',
      affix: '.cast5-cbc'
    });

    var done = this.async();

    if (!options.prefix && !options.affix) {
      throw new Error('Prefix or affix must be set');
    }

    var decryptKey = grunt.option('decrypt') || options.decryptKey;
    // var 
    this.filesSrc.forEach(function(filepath) {

      switch(options.task){
        case  'encrypt':
          var cipher = crypto.createCipher(options.cipher, options.key + options.salt);

          fs.readFile(filepath, 'binary', function(err, text) {
            var encrypted = cipher.update(text, 'binary', 'binary');
            encrypted += cipher.final('binary');

            fs.writeFile(filepath + options.affix, encrypted, 'binary', function() {
              grunt.log.writeln('Encrypted: ' + filepath);
              grunt.log.writeln('Created  : ' + filepath + options.affix);
              done();
            });
          });
          break;

        case 'decrypt':
          var decipher = crypto.createDecipher(options.cipher, options.key + options.salt);

          fs.readFile(filepath, {encoding: 'binary'}, function(err, text) {
            var decrypted = decipher.update(text, 'binary', 'binary');
            decrypted += decipher.final('binary');

            var originalFilepath = filepath.replace(options.affix, '');

            fs.writeFile(originalFilepath, decrypted, 'binary', function() {
              grunt.log.writeln('Decrypted ' + filepath);
              grunt.log.writeln('Created  : ' + originalFilepath);
              done();
            });
          });
          break;

        default: 
          throw new Error('task option required. See readme.');
      }

    });
  });
};
